# U08

## Files:
main.tf<br>
inventory.yml<br>
key<br>
secrets.auto.tfvars<br>
user.yml<br>
cap_setup.yml<br>
ansible.cfg<br>

## Start terraform
Command:<br>
```
terraform init<br>
terraform apply<br>
```
Add the floating ip:s to the inventory file<br>
run: ```ansible -i inventory.yml -m ping all<br>```
It wont work! Go to: Connect floating ip<br>

## Connect floating ip:
### ssh to cap-server:
Command:<br>
```
ssh erik@(ip to cap-server)
```
Command:<br>
```
sudo ip addr add (ip for floating ip to cap-server) dev eth0
exit
```

### ssh to cap-worker
Command:<br>
```
ssh erik@(ip to cap-worker)
```
Command:<br>
```
sudo ip addr add (ip for floating ip to cap-worker) dev eth0
exit<br>
```

Try now and run: ```ansible -i inventory.yml -m ping all```


# Install caprover with ansible
Command:<br>
```
ansible-playbook -i inventory cap_setup.yml
```

# Install Caprover CLI
Command:<br>
```
npm install -g caprover
caprover serversetup
```
Just follow the instructions<br>


To log in:<br>
https://captain.cap.erik.komp.u08.chas.grinton.dev<br>
Password: craprover




### Cluster nodes

ssh to floating ip-cap-server<br>
```
sudo docker swarm join-token worker

```
copy the output<br>
exit<br>

ssh to floating ip-cap-worker<br>
```
sudo docker swarm join --token
```
run the previous output<br>
exit

### Link to the app
http://app-demo.cap.erik.komp.u08.chas.grinton.dev



I followed this guide:<br>
https://caprover.com/docs/ci-cd-integration/deploy-from-gitlab.html<br>
But I'm not abled to do step 5